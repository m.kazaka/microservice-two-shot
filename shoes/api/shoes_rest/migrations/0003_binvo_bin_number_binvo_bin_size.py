# Generated by Django 4.0.3 on 2024-01-31 18:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_binvo_closet_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='binvo',
            name='bin_number',
            field=models.PositiveSmallIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='binvo',
            name='bin_size',
            field=models.PositiveSmallIntegerField(null=True),
        ),
    ]
