from django.db import models
from django.urls import reverse



class BinVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    closet_name = models.CharField(max_length=100, null=True)
    bin_number = models.PositiveSmallIntegerField(null=True)
    bin_size = models.PositiveSmallIntegerField(null=True)


class Shoes(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    picture_url = models.URLField(null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="+",
        on_delete=models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_show_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return self.name

    # class Meta:
    #     ordering = ("model_name",)
