from django.shortcuts import render
from django.http import JsonResponse

from django.views.decorators.http import require_http_methods

import json

from common.json import ModelEncoder
from .models import Shoes, BinVO


class BinVOListEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name",
        "bin_number",
        "id",
        ]

class BinVOEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_size",
        "bin_number",
        "import_href",
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
]
    encoders = {
        "bin": BinVOEncoder(),
    }

class ShoeListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVOListEncoder(),
    }



@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_shoes(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.get(id=pk)

        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeDetailEncoder,
            safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)

    Shoes.objects.filter(id=pk).update(**content)
    shoes = Shoes.objects.get(id=pk)
    return JsonResponse(
        shoes,
        encoder=ShoeDetailEncoder,
        safe=False,
    )




# def api_shoes_create

@require_http_methods(["GET", "POST"])
def api_shoes_list(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoes.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoes.objects.all()
        return JsonResponse(
            {'shoes': shoes},
            encoder=ShoeListEncoder
        )
    else:
        content = json.loads(request.body)

        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoes.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )