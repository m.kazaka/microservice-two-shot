from django.urls import path

from .api_views import (
    api_show_shoes,
    api_shoes_list,
)

urlpatterns = [
    path("shoes/<int:pk>/", api_show_shoes, name="api_show_shoes"),
    path("shoes/", api_shoes_list, name="api_shoes_list"),
    path(
        "bins/<int:bin_vo_id>/shoes/",
        api_shoes_list,
        name="api_shoes_create"),
]