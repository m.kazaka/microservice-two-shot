import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ShoesList from './ShoesList';
import ShoesForm from './ShoesForm';
import UpdateShoeForm from './UpdateShoe';

function App() {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path = '/shoes'>
            <Route index element={<ShoesList />} />
            <Route path="new" element={<ShoesForm />} />
            <Route path="update" element={<UpdateShoeForm />} />
          </Route>

          <Route
          />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
