import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function UpdateShoeForm({ shoe }) {

    const navigate = useNavigate();

    const [bins, setBins] = useState([]);
    const [formData, setFormData] = useState({
        manufacturer: '',
        model_name: '',
        color: '',
        bin: '',
    });

    const getData = async () => {
        const url = 'http://localhost:8100/api/shoes/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    };

    useEffect(() => {
        getData();

        setFormData({
            manufacturer: shoe.manufacturer || '',
            model_name: shoe.model_name || '',
            color: shoe.color || '',
            bin: shoe.bin || '',
        });
    }, [shoe]);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const { binId } = formData;
        // const {shoeId} = formData
        const url = `http://localhost:8080/api/bins/${binId}/`;
        // const url = `http://localhost:8080/api/shoes/${shoeId}/`;


        const fetchConfig = {
            method: "put",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {

            navigate.push('/shoeList');
        }
    };

    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value,
        });
    };
    return (
  <>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Update Shoes</h1>
          <form onSubmit={handleSubmit} id="update-shoe-form">
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.manufacturer}
              placeholder="Manufacturer"
              required name="manufacturer"
              className="form-control" />
              <label htmlFor="manufacturer">Brand name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.model_name}
              placeholder="Model name"
              required name="model_name"
              type="text"
              id="model_name"
              className="form-control" />
              <label htmlFor="model_name">Model name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleFormChange}
              value={formData.color}
              placeholder="Color"
              type="text"
              name="color"
              id="color"
              className="form-control" />
              <label htmlFor="color">Color</label>
            </div>
            <div className="mb-3">
              <select onChange={handleFormChange}
              value={formData.bin}
              required name="bin"
              id="bin"
              className="form-select" >
              <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.id} value={bin.id}>{bin.closet_name}</option>
                  );
                })}
            </select>
            </div>
            <button className="btn btn-primary">Update</button>
          </form>
        </div>
      </div>
    </div>
    </>
    )

}

export default UpdateShoeForm;