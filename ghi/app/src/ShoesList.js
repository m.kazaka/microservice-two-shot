import React, { useEffect, useState } from "react";
import { Link } from 'react-router-dom';
import './ShoeList.css';

function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const getData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/shoes/');

            if (response.ok) {
                const data = await response.json();
                setShoes(data.shoes);
            } else {
                throw new Error('Failed to fetch data');
            }
        } catch (error) {
            console.error('Error fetching shoe data:', error);
        }
    };

    const deleteShoe = async (shoeId) => {
        const confirmDelete = window.confirm("Are you sure you want to delete this shoe?");
        if (!confirmDelete) {
          return;
        }

        try {
          const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}`, {
            method: 'DELETE',
          });

          if (response.ok) {
            setShoes(shoes.filter(shoe => shoe.id !== shoeId));
          } else {
            throw new Error('Failed to delete shoe');
          }
        } catch (error) {
          console.error('Error deleting shoe:', error);
        }
      };


    useEffect(()=>{
        getData()
    }, [])

    return (

        <>
        <div className="row">
            {shoes.map(shoe => {
              return (
        <div key={shoe.href} className="card w-25 m-3 mb-3 shadow">
          <div className="image">
            <img width="200" className="card-img-top bg-white rounded shadow d-block mx-auto mb-4" src={ shoe.picture_url } alt="Card image cap" />
            </div>
            <div className= "card-body">
                <h5 className="card-title">{ shoe.manufacturer }</h5>
                <h6 className="card-title">{ shoe.model_name }</h6>
                <p className="card-text">Closet: { shoe.bin.closet_name }</p>
                <p className="card-text">Bin: { shoe.bin.bin_number }</p>
                <Link to={`/shoes/update/${shoe.id}/`}>
                    <button id="style-2" className="shoe_list"  data-replace="NAH!">Update</button>
                </Link>
                <h4></h4>
                <button className="shoe_list"  onClick={() => deleteShoe(shoe.id)} id="style-2" data-replace="YEEET!" ><span>Delete</span></button>
            </div>
        </div>
            );
          })}
      </div>
        </>
    )
  }

  export default ShoesList;
