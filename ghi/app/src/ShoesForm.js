import React, {useEffect, useState} from 'react';


function ShoesForm() {


    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState ({
        manufacturer: '',
        model_name: '',
        color:'',
        picture_url: '',
        bin:'',
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const {bin} = formData;
        const url = `http://localhost:8080${bin}shoes/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                picture_url: '',
                bin: '',
              });
              console.log(setFormData);
        }
    }

    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
  <>
    <div>
          <h1>Add Shoes</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
            <div className="mb-3">
              <label htmlFor="manufacturer" className='form-label'>Brand name</label>
              <input onChange={handleFormChange}
              value={formData.manufacturer}
              placeholder="ex; Nike"
              required name="manufacturer"
              className="form-control" />
            </div>
            <div className="mb-3">
              <label htmlFor="model_name">Model name</label>
              <input onChange={handleFormChange}
              value={formData.model_name}
              placeholder="ex; Jordan Retro"
              required name="model_name"
              type="text"
              id="model_name"
              className="form-control" />
            </div>
            <div className="mb-3">
              <label htmlFor="color">Color</label>
              <input onChange={handleFormChange}
              value={formData.color}
              placeholder="ex; multi, black, off-white"
              type="text"
              name="color"
              id="color"
              className="form-control" />
            </div>
            <div className="mb-3">
              <label htmlFor="picture_url">Picture Url</label>
              <input onChange={handleFormChange}
              value={formData.picture_url}
              type="text"
              name="picture_url"
              id="picture_url"
              className="form-control" />
            </div>
            <div className="mb-3">
              <label htmlFor='bin' className='form-label'>Bin</label>
              <select onChange={handleFormChange}
              value={formData.bin}
              required name="bin"
              id="bin"
              className="form-select" >
              <option value="">Choose a bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.href} value={bin.href}>{bin.id}</option>
                  );
                })}
            </select>
            </div>
            <button className="btn btn-primary" id="style-2" data-replace="" ><span>Add</span></button>
          </form>
    </div>
    </>
    )

}

export default ShoesForm;