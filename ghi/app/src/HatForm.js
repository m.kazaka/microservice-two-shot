import React, {useEffect, useState} from 'react';


function HatForm() {


    const [location, setLocation] = useState([])
    const [forData, setFormdata] = useState ({
        manufacturer: '',
        model_name: '',
        color:'',
        location:'',
    })

    const getData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.location);
        }
    }

    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDeafault();

        const {location} = formData;
        const url = `http://localhost:8080/api/location/${location}/hats/`

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                manufacturer: '',
                model_name: '',
                color: '',
                location: '',
            });
        }
    }

    const handleFormChange = (e) => {
        const inputName = e.target.name;
        const value = e.target.value;
        setFormData({
            ...forData,
            [inputName]: value
        });
    }

}